package ru.nexign.call.processor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nexign.call.processor.model.Event;
import ru.nexign.call.processor.model.EventType;

import java.time.LocalDateTime;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    int countEventsBySubsIdAndCreatedDateAfterAndType(long subsId, LocalDateTime date, EventType type);
}
