package ru.nexign.call.processor.dto;

import lombok.Data;

@Data
public class StatusDto {

    private double balance;
    private Status status;
}
