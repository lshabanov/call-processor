package ru.nexign.call.processor.dto;

public enum Status {

    ACTIVE,
    BLOCKED
}
