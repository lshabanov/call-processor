package ru.nexign.call.processor.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "tariff_plan")
public class TariffPlan {

    @Id
    private int id;

    /**
     * Стомость СМС
     */
    @Column(name = "sms_cost")
    private float smsCost;

    /**
     * Стоимость звонка
     */
    @Column(name = "call_cost")
    private float callCost;
}
