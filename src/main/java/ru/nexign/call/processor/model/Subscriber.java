package ru.nexign.call.processor.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "subscriber")
public class Subscriber {

    @Id
    private Long id;

    /**
     * Имя абонента
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Фамилия абонента
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Номер телефона
     */
    @Column(name = "msisdn")
    private String msisdn;

    /**
     * Текущий баланс
     */
    @Column(name = "balance")
    private float balance;

    /**
     * Тарифный план
     */
    @ManyToOne
    @JoinColumn
    private TariffPlan tariffPlan;

    /**
     * Флаг блокировки клиента
     */
    @Column(name = "blocked")
    private boolean blocked;
}
