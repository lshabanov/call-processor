package ru.nexign.call.processor.exception;

public class SubscriberBlockedException extends RuntimeException {

    public SubscriberBlockedException() {

    }

    public SubscriberBlockedException(String message) {
        super(message);
    }
}
