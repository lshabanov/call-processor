package ru.nexign.call.processor.exception;

public class CallsLimitException extends RuntimeException {

    public CallsLimitException() {

    }

    public CallsLimitException(String message) {
        super(message);
    }
}
