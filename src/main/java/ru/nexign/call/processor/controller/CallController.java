package ru.nexign.call.processor.controller;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nexign.call.processor.service.CallService;

@RestController
@RequiredArgsConstructor
public class CallController implements BaseController {

    private final CallService callService;

    @PostMapping(value = "/subscribers/{subsId}/call")
    @ApiOperation(value = "Make call")
    public void call(@PathVariable long subsId) {
        callService.makeCall(subsId);
    }

    @PostMapping(value = "/subscribers/{subsId}/sms/send")
    @ApiOperation(value = "Send sms")
    public void sendSms(@PathVariable long subsId) {
        callService.sendSms(subsId);
    }
}
