package ru.nexign.call.processor.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nexign.call.processor.dto.StatusDto;
import ru.nexign.call.processor.service.BalanceService;

import javax.validation.constraints.Positive;

@RestController
@RequiredArgsConstructor
@Validated
@Api(value = "balance api", description = "Operations with subscribers balance")
public class BalanceController implements BaseController {

    private final BalanceService balanceService;

    @GetMapping(value = "/subscribers/{subsId}/status")
    @ApiOperation(value = "Check current balance and status", response = StatusDto.class)
    public StatusDto checkBalance(@PathVariable long subsId) {
        return balanceService.checkBalance(subsId);
    }

    @PutMapping(value = "/subscribers/{subsId}/balance/recharge")
    @ApiOperation(value = "Recharge balance")
    public void recharge(@PathVariable long subsId,
                         @RequestParam @Positive float amount) {
        balanceService.recharge(subsId, amount);
    }

}
