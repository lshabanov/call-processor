package ru.nexign.call.processor.controller.advice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.nexign.call.processor.config.ExceptionConfig;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class ExceptionControllerAdvice {

    private final ExceptionConfig exceptionConfig;

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map<String, String> handleNotFound(HttpServletRequest request, Exception exception) {
        return handle(request, exception);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, String> handleUnknown(HttpServletRequest request, Exception exception) {
        return handle(request, exception);
    }

    private Map<String, String> handle(HttpServletRequest request, Exception exception) {
        log.error("Exception at " + request.getRequestURI(), exception);
        Map<String, String> map = new HashMap<>();
        map.put("message", exception.getMessage());
        if (exceptionConfig.isShowStackTrace()) {
            map.put("stackTrace", getStackTrace(exception));
        }
        return map;
    }

    private String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }

}
