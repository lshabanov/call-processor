package ru.nexign.call.processor.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.nexign.call.processor.config.CallsConfig;
import ru.nexign.call.processor.exception.CallsLimitException;
import ru.nexign.call.processor.exception.SubscriberBlockedException;
import ru.nexign.call.processor.model.Event;
import ru.nexign.call.processor.model.EventType;
import ru.nexign.call.processor.model.Subscriber;
import ru.nexign.call.processor.repository.EventRepository;
import ru.nexign.call.processor.repository.SubscriberRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;

import static ru.nexign.call.processor.model.EventType.CALL;
import static ru.nexign.call.processor.model.EventType.SMS;

@Service
@RequiredArgsConstructor
@Slf4j
public class CallService {

    private final SubscriberRepository subsRepository;
    private final EventRepository eventRepository;
    private final CallsConfig callsConfig;

    /**
     * Совершить звонок
     *
     * @param subsId идентификатор абонента
     */
    @Transactional
    public void makeCall(long subsId) {
        Subscriber subscriber = subsRepository.getOne(subsId);
        validate(subscriber, CALL);

        log.info("Making call...");

        updateBalance(subscriber, subscriber.getTariffPlan().getCallCost());
        subsRepository.save(subscriber);
        eventRepository.save(createEvent(subsId, CALL));
    }

    /**
     * Отправить смс
     *
     * @param subsId идентификатор абонента
     */
    @Transactional
    public void sendSms(long subsId) {
        Subscriber subscriber = subsRepository.getOne(subsId);
        validate(subscriber, SMS);

        log.info("Sending sms...");

        updateBalance(subscriber, subscriber.getTariffPlan().getSmsCost());
        subsRepository.save(subscriber);
        eventRepository.save(createEvent(subsId, SMS));
    }

    private Event createEvent(Long subsId, EventType type) {
        Event event = new Event();
        event.setSubsId(subsId);
        event.setType(type);
        return event;
    }

    private void updateBalance(Subscriber subscriber, float cost) {
        subscriber.setBalance(subscriber.getBalance() - cost);
        if (subscriber.getBalance() < 0.0) {
            log.debug("Subscriber {} has negative balance. He will be blocked.", subscriber.getId());
            subscriber.setBlocked(true);
        }
    }

    private void validate(Subscriber subscriber, EventType type) {
        if (subscriber.isBlocked() || subscriber.getBalance() < 0d) {
            throw new SubscriberBlockedException(
                    String.format("Subscriber %s is blocked. Recharge balance.", subscriber.getId()));
        }
        if (CALL == type) {
            int callsCountToday = eventRepository
                    .countEventsBySubsIdAndCreatedDateAfterAndType(subscriber.getId(), LocalDate.now().atStartOfDay(), CALL);
            log.info("Subs {} has {} calls today", subscriber.getId(), callsCountToday);
            if (callsCountToday >= callsConfig.getLimit()) {
                throw new CallsLimitException(
                        String.format("Subscriber %s exceeded the call limit for today", subscriber.getId()));
            }

        }
    }
}
