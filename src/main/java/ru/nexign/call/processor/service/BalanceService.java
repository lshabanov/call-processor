package ru.nexign.call.processor.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nexign.call.processor.dto.StatusDto;
import ru.nexign.call.processor.mapper.StatusDtoMapper;
import ru.nexign.call.processor.model.Subscriber;
import ru.nexign.call.processor.repository.SubscriberRepository;

@Service
@RequiredArgsConstructor
@Slf4j
public class BalanceService {

    private final SubscriberRepository repository;
    private final StatusDtoMapper mapper;

    /**
     * Пополнить баланс абонента
     *
     * @param subsId идентификатор абонента
     * @param amount сумма пополнения
     */
    public void recharge(long subsId, float amount) {
        log.info("Balance recharging by {} for subs {}", amount, subsId);
        Subscriber subscriber = repository.getOne(subsId);

        float currentBalance = subscriber.getBalance();
        subscriber.setBalance(currentBalance + amount);

        if (subscriber.isBlocked() && subscriber.getBalance() >= 0.0) {
            log.info("Subscriber {} unlocked", subscriber.getId());
            subscriber.setBlocked(false);
        }
        repository.save(subscriber);
    }

    /**
     * Проверить текущий баланс и статус абонента
     *
     * @param subsId идентификатор абонента
     * @return {@link StatusDto}
     */
    @Transactional(readOnly = true)
    public StatusDto checkBalance(long subsId) {
        log.debug("Checking the balance of the subs {}", subsId);
        Subscriber subscriber = repository.getOne(subsId);
        return mapper.mapSubscriberToStatusDto(subscriber);
    }
}
