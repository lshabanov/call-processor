package ru.nexign.call.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class CallProcessorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CallProcessorApplication.class, args);
    }
}
