package ru.nexign.call.processor.mapper;

import org.springframework.stereotype.Component;
import ru.nexign.call.processor.dto.StatusDto;
import ru.nexign.call.processor.model.Subscriber;

import static ru.nexign.call.processor.dto.Status.ACTIVE;
import static ru.nexign.call.processor.dto.Status.BLOCKED;

@Component
public class StatusDtoMapper {

    public StatusDto mapSubscriberToStatusDto(Subscriber subscriber) {
        StatusDto statusDto = new StatusDto();
        statusDto.setBalance(subscriber.getBalance());
        statusDto.setStatus(subscriber.isBlocked() ? BLOCKED : ACTIVE);
        return statusDto;
    }
}
