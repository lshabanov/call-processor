package ru.nexign.call.processor.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "exception")
@Data
public class ExceptionConfig {

    private boolean showStackTrace;
}
