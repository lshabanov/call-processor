package ru.nexign.call.processor.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.nexign.call.processor.config.CallsConfig;
import ru.nexign.call.processor.exception.CallsLimitException;
import ru.nexign.call.processor.exception.SubscriberBlockedException;
import ru.nexign.call.processor.model.Subscriber;
import ru.nexign.call.processor.model.TariffPlan;
import ru.nexign.call.processor.repository.EventRepository;
import ru.nexign.call.processor.repository.SubscriberRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CallServiceTest {

    @InjectMocks
    private CallService callService;

    @Mock
    private SubscriberRepository subsRepository;

    @Mock
    private EventRepository eventRepository;

    @Mock
    private CallsConfig config;

    @Before
    public void setUp() {
        when(subsRepository.getOne(anyLong())).thenReturn(generateSubscriber());
        when(config.getLimit()).thenReturn(3);
    }

    @Test
    public void makeCall() {
        callService.makeCall(1L);

        verify(subsRepository).save(any());
        verify(eventRepository).save(any());
    }

    @Test(expected = CallsLimitException.class)
    public void testCallsLimit() {
        when(config.getLimit()).thenReturn(0);
        callService.makeCall(1L);
    }

    @Test(expected = SubscriberBlockedException.class)
    public void testBlockedSubs() {
        Subscriber subscriber = generateSubscriber();
        subscriber.setBlocked(true);
        when(subsRepository.getOne(anyLong())).thenReturn(subscriber);

        callService.makeCall(1L);
    }

    @Test(expected = SubscriberBlockedException.class)
    public void testNegativeBalance() {
        Subscriber subscriber = generateSubscriber();
        subscriber.setBalance(-1.0f);
        when(subsRepository.getOne(anyLong())).thenReturn(subscriber);

        callService.makeCall(1L);
    }

    @Test
    public void sendSms() {
        callService.sendSms(1L);

        verify(subsRepository).save(any());
        verify(eventRepository).save(any());
    }

    private Subscriber generateSubscriber() {
        Subscriber subscriber = new Subscriber();
        subscriber.setId(1L);
        subscriber.setTariffPlan(new TariffPlan());
        return subscriber;
    }
}