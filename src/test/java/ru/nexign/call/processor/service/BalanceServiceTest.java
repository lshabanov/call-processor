package ru.nexign.call.processor.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.nexign.call.processor.mapper.StatusDtoMapper;
import ru.nexign.call.processor.model.Subscriber;
import ru.nexign.call.processor.repository.SubscriberRepository;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BalanceServiceTest {

    @Mock
    private SubscriberRepository repository;

    @Mock
    private StatusDtoMapper mapper;

    @InjectMocks
    private BalanceService balanceService;

    @Test
    public void recharge() {
        Subscriber subscriber = new Subscriber();
        subscriber.setId(1L);
        subscriber.setBalance(2.2f);

        when(repository.getOne(any())).thenReturn(subscriber);

        balanceService.recharge(1L, 3.3f);

        verify(repository).save(any());
        assertEquals(5.5, subscriber.getBalance(), 0.01);
    }

    @Test
    public void checkBalance() {
        Subscriber subscriber = new Subscriber();
        when(repository.getOne(1L)).thenReturn(subscriber);
        balanceService.checkBalance(1L);

        verify(repository).getOne(1L);
        verify(mapper).mapSubscriberToStatusDto(subscriber);
    }
}