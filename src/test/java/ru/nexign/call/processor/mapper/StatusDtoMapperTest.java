package ru.nexign.call.processor.mapper;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import ru.nexign.call.processor.dto.Status;
import ru.nexign.call.processor.dto.StatusDto;
import ru.nexign.call.processor.model.Subscriber;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
class StatusDtoMapperTest {

    private StatusDtoMapper mapper = new StatusDtoMapper();

    @Test
    void mapActiveSubscriberToStatusDto() {
        Subscriber subscriber = new Subscriber();
        subscriber.setBalance(9.88f);
        subscriber.setBlocked(false);

        StatusDto statusDto = mapper.mapSubscriberToStatusDto(subscriber);

        assertNotNull(statusDto);
        assertEquals(subscriber.getBalance(), statusDto.getBalance(), 0.01);
        assertEquals(Status.ACTIVE, statusDto.getStatus());
    }

    @Test
    void mapBlockedSubscriberToStatusDto() {
        Subscriber subscriber = new Subscriber();
        subscriber.setBalance(-9.88f);
        subscriber.setBlocked(true);

        StatusDto statusDto = mapper.mapSubscriberToStatusDto(subscriber);

        assertNotNull(statusDto);
        assertEquals(subscriber.getBalance(), statusDto.getBalance(), 0.01);
        assertEquals(Status.BLOCKED, statusDto.getStatus());
    }
}
