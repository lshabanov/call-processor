Call processor application
==========================

* Требования: java 1.8 or greater, maven, postgres

* Запуск приложения: mvn spring-boot:run

* Перед запуском необходимо указать настройки БД (postgres) в application.yml

* Инициализация таблиц и их наполнение происходит автоматически средствами liquibase
(change logs path: /src/main/resources/db/changelog)

* Данные для заплонения таблиц: /src/main/resources/db/data

* Сваггер документация api: http://{host}:{port}/swagger-ui.html#

TODO:
В связи с ограниченным кол-ом времени в приложении остались такие проблемы как:

* малое кол-во кейсов unit тестов
* неполная документация
* нет округления float значений в БД
* возможно наличие багов (протестирован только основной функционал)
